# Ekdosis4Stylo

Le projet est d'arriver à une version de Stylo qui supporte ekdosis.

## Premier test

Ici j'ai commencé un premier test. 

L'idée est de produire une édition d'exemple du sonnet proposé par Robert.

Il y a un `template.late` qui contient le préambule et toutes les informations nécessaires pour ekdosis. Dans le body on mettra seulement les vers.

J'ai utilisé la syntaxe  `[[might][Gil, Se, Ew: may]]`

Ensuite j'utilise le filtre https://github.com/kprussing/pandoc-div2env pour prendre en compte les environnements.

Je parse le texte md pour remplacer la syntaxe avec les bonnes balises ekdosis. Évidemment on pourra faire un parseur plus sérieux.

Le script `compile.sh` produit donc le `pdf` et l'`xml` à partir du `.md` 

On peut facilement mettre en place la même chose dans l'export stylo.


