
::: {data-environment=ekdstanza data-environment-keyword="type=quatrain"}
From fairest creatures we desire increase,    
That thereby beauties _Rose_ [[might][Gil, Se, Ew: may]]   
neuer die,   
But as the riper should by time decease,   
His tender heire might beare his memory:   
:::

::: {data-environment=ekdstanza data-environment-keyword="type=quatrain"}

Thou that art now the worlds fresh ornament,   
And only herauld to the gaudy spring,    
Within thine owne bud buriest thy content,    
And tender chorle makst wast in niggarding:   
:::

::: {data-environment=ekdstanza data-environment-keyword="type=quatrain"}

Thou that art now the worlds fresh ornament,    
And only herauld to the gaudy spring,    
Within thine owne bud buriest thy content,    
And tender chorle makst wast in niggarding:    

:::

::: {data-environment=ekdstanza data-environment-keyword="type=couplet"}

Pitty the world,or else this glutton be,    
To eate the worlds due,by the grave and thee.    
:::




