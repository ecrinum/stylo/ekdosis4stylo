\documentclass[12pt,a4paper]{article}
\usepackage[english]{babel}

% On demande à ekdosis l'export TEI xml, puis d'utiliser tidy pour le
% reformater. Pour permettre à ekdosis d'appeler tidy après l'export,
% il faut compiler avec `lualatex --shell-escape`. On utilise aussi
% les commandes avancées pour la poésie.
\usepackage[teiexport=tidy,
            poetry=verse]{ekdosis}

% Déclaration des sources: trois éditions. Dans le premier argument le
% sigle (qui sera aussi un xml:id) et dans le second son rendu dans la
% version pour l'impression. À savoir une lettre initiale suivie d'une
% commande de citation sans output mais qui servira à produire la
% liste des sigles utilisés, ou encore une bibliographie à la fin.
\DeclareSource{Gil}{G\notecite{Gil}}
\DeclareSource{Se}{S\notecite{Se}}
\DeclareSource{Ew}{E\notecite{Ew}}

% Mise en forme des divisions du document. Comprendre ici que le
% niveau 1 doit faire apparaître le numéro du sonnet centré et entouré
% par --.
\FormatDiv{1}{%
  \nolinenumbers\begin{center}-- }{ --\end{center}\linenumbers}

% Mise en forme de l'apparat critique: les numéros des lignes en
% romain et suivis d'un point. Le lemme en gras et les variantes en
% italique.
\SetHooks{
  refnumstyle=\normalfont,
  postrefnum=.~,
  lemmastyle=\bfseries,
  readingstyle=\itshape
}

% On demande à ekdosis d'insérer les références aux sources appelées
% par leur xmd:id dans la sortie TEI. Le fichier references.xml a été
% produit par zotero et sera transformé par ekdosis. On ne saisit donc
% les références qu'une seule fois dans zotero.
\AddxmlBibResource{references.xml}

% Appel normal de biblatex
\usepackage{csquotes}
\usepackage[style=oxyear]{biblatex}
\addbibresource{references.bib}

\begin{document}
% Impression automatique de la liste des sigles utilisés:
\printbiblist[title=Sources]{shorthand}

% Ci-dessous:
% - \ekddiv est pour la table des sonnets. Dans le TEI, ce sera <div>
% - \indentpattern est pour la mise en forme: douze zéros car les trois
%   quatrains ne sont pas indentés et deux fois le chiffre 1 pour
%   indenter de 1 unité les deux vers du couplet à la fin.

\begin{ekdosis}
  \ekddiv{type=sonnet,head=1,n=1}
  \begin{ekdverse}[type=sonnet]
	  $body$
  \end{ekdverse}
\end{ekdosis}
\end{document}
