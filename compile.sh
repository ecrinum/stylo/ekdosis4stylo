python3 lem.py $1.md
pandoc -f markdown -t latex --filter pandoc-div2env temp.md --template=template.latex -o $1.tex
lualatex $1.tex
lualatex $1.tex
biber $1
lualatex $1.tex
biber $1
lualatex $1.tex
rm temp.md
rm $1.aux
rm $1.bcf
rm $1.bbl
rm $1.blg
rm $1.ekd
rm $1.log
rm $1.run.xml

