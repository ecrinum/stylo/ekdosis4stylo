function Div (element)
	if element.classes[1] == 'variant' then
		return {
			pandoc.RawInline('tex', '\\begin{ekdosis}'),
			element,
		 	pandoc.RawInline('tex', '\\end{ekdosis}'),
		}
	end
end
