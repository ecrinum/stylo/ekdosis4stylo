---
title: "Test 1"
header-includes: |
    \usepackage{ekdosis}
---

##  Test de LaTeX inline

Ceci est un paragraphe en Markdown.

Ceci est un paragraphe md avec une \emph{emphase} en latex.

::: variant
I met my friend app{
    \lem{Peter}
    \rdg{John}
    } at the station yesterday.
:::

